```plantuml

@startuml Abstract factory with Observer

interface IObserver {
    + Update(int data)
}

interface IAbstractFactory {
    + CreateProductA(observer: IObserver): IProductA
    + CreateProductB(observer: IObserver): IProductB
}

interface IProductA {
    + DoSomethingA()
    + RemoveObs()
    + RegisterObs(observer: IObserver)
}

interface IProductB {
    + DoSomethingB()
    + RemoveObs()
    + RegisterObs(observer: IObserver)
}

class TechFactory {
    + CreateProductA(observer: IObserver): IProductA
    + CreateProductB(observer: IObserver): IProductB
}

class VehicleFactory {
    + CreateProductA(observer: IObserver): IProductA
    + CreateProductB(observer: IObserver): IProductB
}

abstract class ObservableProduct {
    - data: int
    - observer: IObserver
    + SetData(data: int)
    + RegisterObserver(observer: IObserver)
    + RemoveObserver()
    - NotifyObserver()
}

class ConcreteObserver {
    + Update(data: int)
}


class Computer {
    + Computer(observer: IObserver)
    + RegisterObs(observer: IObserver)
    + RemoveObs()
    + DoSomethingA()
}

class MobilePhone {
    + MobilePhone(observer: IObserver)
    + RegisterObs(observer: IObserver)
    + RemoveObs()
    + DoSomethingB()
}

class Car {
    + Car(observer: IObserver)
    + RegisterObs(observer: IObserver)
    + RemoveObs()
    + DoSomethingA()
}

class Truck {
    + Truck(observer: IObserver)
    + RegisterObs(observer: IObserver)
    + RemoveObs()
    + DoSomethingB()
}

IAbstractFactory <|.. TechFactory
IAbstractFactory <|.. VehicleFactory
TechFactory --> Computer
TechFactory --> MobilePhone
VehicleFactory --> Car
VehicleFactory --> MobilePhone
Computer --|> ObservableProduct
MobilePhone --|>  ObservableProduct
Car --|>  ObservableProduct
Truck --|>  ObservableProduct

IProductA <|.. Computer
IProductA <|.. Car
IProductB <|.. MobilePhone
IProductB <|.. Truck

IObserver <|-- ConcreteObserver

@enduml
