﻿using System;
using System.Collections.Generic;

interface Product
{
    void UpdateData(string data);
}

interface IObserver
{
    void Update(string data);
}

interface ISubject
{
    void Subscribe(IObserver observer);
    void UnSubscribe(IObserver observer);
    void Notify(string data);
}

class Car : ISubject, Product
{
    private string data;
    private List<IObserver> observers = new List<IObserver>();

    public void Subscribe(IObserver observer)
    {
        observers.Add(observer);
    }

    public void UnSubscribe(IObserver observer)
    {
        observers.Remove(observer);
    }

    public void Notify(string data)
    {
        foreach (var observer in observers)
        {
            observer.Update(data);
        }
    }

    public void UpdateData(string data)
    {
        this.data = data;
        Console.WriteLine("Car data updated: " + this.data);
        Notify(data);
    }
}

class Truck : ISubject, Product
{
    private string data;
    private List<IObserver> observers = new List<IObserver>();

    public void Subscribe(IObserver observer)
    {
        observers.Add(observer);
    }

    public void UnSubscribe(IObserver observer)
    {
        observers.Remove(observer);
    }

    public void Notify(string data)
    {
        foreach (var observer in observers)
        {
            observer.Update(data);
        }
    }

    public void UpdateData(string data)
    {
        this.data = data;
        Console.WriteLine("Truck data updated: " + this.data);
        Notify(data);
    }
}

class MobilePhone : IObserver, Product
{
    public void Update(string data)
    {
        Console.WriteLine("Mobile phone updated: " + data);
    }

    public void UpdateData(string data)
    {

    }
}

class Computer : IObserver, Product
{
    public void Update(string data)
    {
        Console.WriteLine("Computer updated: " + data);
    }

    public void UpdateData(string data)
    {

    }
}

// Абстрактная фабрика
interface IAbstractFactory
{
    Product CreateProductA();
    Product CreateProductB();
}

// Фабрика VehicleFactory
class VehicleFactory : IAbstractFactory
{
    public Product CreateProductA()
    {
        return new Car();
    }
    public Product CreateProductB()
    {
        return new Truck();
    }
}

// Фабрика TechFactory
class TechFactory : IAbstractFactory
{
    public Product CreateProductA()
    {
        return new MobilePhone();
    }
    public Product CreateProductB()
    {
        return new Computer();
    }

}

class Program
{
    static void Main(string[] args)
    {
        IAbstractFactory vehicleFactory = new VehicleFactory();
        IAbstractFactory techFactory = new TechFactory();

        Product car = vehicleFactory.CreateProductA();
        Product truck = vehicleFactory.CreateProductB();
        Product mobilePhone1 = techFactory.CreateProductA();
        Product mobilePhone2 = techFactory.CreateProductA();
        Product computer = techFactory.CreateProductB();

        IObserver mobilePhoneObserver1 = (IObserver)mobilePhone1;
        IObserver mobilePhoneObserver2 = (IObserver)mobilePhone2;
        IObserver computerObserver = (IObserver)computer;

        ((ISubject)car).Subscribe(mobilePhoneObserver1);
        ((ISubject)car).Subscribe(mobilePhoneObserver2);
        ((ISubject)truck).Subscribe(computerObserver);

        car.UpdateData("1");
        Console.WriteLine();
        truck.UpdateData("2");
        Console.WriteLine();

        ((ISubject)car).UnSubscribe(mobilePhoneObserver1);
        ((ISubject)truck).UnSubscribe(computerObserver);

        car.UpdateData("3");
        Console.WriteLine();
        truck.UpdateData("4");
        Console.WriteLine();

        Console.Read();
    }
}
